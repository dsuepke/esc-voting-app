/*
 * Copyright (c) 2023 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import net.suepke.eurovision.config.Country;
import net.suepke.eurovision.config.Player;
import net.suepke.eurovision.data.VoteDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ContestController.class)
class ContestControllerTests {

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    void before() {
        ContestState.reset();
    }

    @Test
    @SneakyThrows
    void contestTest() {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/contest")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(loadResourceAsString("contestState_initial.json")));
    }

    @Test
    @SneakyThrows
    void contestTestWithShowCurrentVotes() {
        ContestState.showCurrentVotes = true;
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/contest")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(loadResourceAsString("contestState_showCurrentVotes.json")));
    }

    @Test
    void countriesTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/countries/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(Country.values().length)))
                .andExpect(jsonPath("$[0]", is("SWEDEN")))
                .andExpect(jsonPath("$[24]", is("AUSTRIA")));
    }

    @Test
    void countryNextTest() throws Exception {
        assertThat(ContestState.currentCountry, is(Country.values()[0]));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/countries/current/next")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(ContestState.currentCountry, is(Country.values()[1]));
    }


    @Test
    void countryPreviousTest() throws Exception {
        assertThat(ContestState.currentCountry, is(Country.values()[0]));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/countries/current/previous")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(ContestState.currentCountry, is(Country.values()[0]));

        // TODO: How to test this without mixing requests?
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/countries/current/next")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(ContestState.currentCountry, is(Country.values()[1]));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/countries/current/previous")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(ContestState.currentCountry, is(Country.values()[0]));
    }

    @Test
    void playersTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/players/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(Player.values().length)))
                .andExpect(jsonPath("$[0]", is("DANIEL")))
                .andExpect(jsonPath("$[6]", is("ARP")));
    }


    @Test
    void addVoteTest() throws Exception {
        VoteDto vote = new VoteDto(Player.DANIEL.name(), Country.GERMANY, 5);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/votes/vote")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new Gson().toJson(vote)))
                .andExpect(status().isOk());

        assertThat(ContestState.currentCountry, is(Country.values()[0]));
    }

    @Test
    void showCurrentVotesTest() throws Exception {
        assertThat(ContestState.showCurrentVotes, is(false));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/votes/showCurrent")
                        .content("player: DANIEL"))
                .andExpect(status().isOk());

        assertThat(ContestState.showCurrentVotes, is(true));
    }

    @Test
    void playerAfkTest() throws Exception {
        assertThat(ContestState.afkPlayers, hasSize(0));

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/players/DANIEL/afk")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("true"))
                .andExpect(status().isOk());

        assertThat(ContestState.afkPlayers, hasSize(1));
    }

    @Test
    void resetTest() throws Exception {
        assertThat(ContestState.votes[0][0], is(0));
        assertThat(ContestState.currentCountry, is(Country.SWEDEN));
        assertThat(ContestState.showCurrentVotes, is(false));

        ContestState.votes[0][0] = 5;
        ContestService.nextCountry();
        ContestService.showCurrentVotes();

        assertThat(ContestState.votes[0][0], is(not(0)));
        assertThat(ContestState.currentCountry, is(not(Country.values()[0])));
        assertThat(ContestState.showCurrentVotes, is(not(false)));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/reset")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(ContestState.votes[0][0], is(0));
        assertThat(ContestState.currentCountry, is(Country.values()[0]));
        assertThat(ContestState.showCurrentVotes, is(false));
    }

    @SneakyThrows
    @SuppressWarnings("DataFlowIssue")
    public static String loadResourceAsString(String resourcePath) {
        return Files.readString(Paths.get(ClassLoader.getSystemClassLoader().getResource(resourcePath).toURI()), StandardCharsets.UTF_8);
    }
}
