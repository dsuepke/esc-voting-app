package net.suepke.eurovision;

import lombok.extern.slf4j.Slf4j;
import net.suepke.eurovision.config.Country;
import net.suepke.eurovision.config.Player;
import net.suepke.eurovision.data.ContestUpdateDto;
import net.suepke.eurovision.data.VoteDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@CrossOrigin(origins = {"http://localhost:63342", "https://esc.suepke.net"})
@RestController
public class ContestController {

    private final CopyOnWriteArrayList<SseEmitter> emitters = new CopyOnWriteArrayList<>();

    @GetMapping("/countries/")
    public Collection<String> countries() {
        return Arrays.stream(Country.values()).map(Enum::toString).toList();
    }

    @PutMapping("/countries/current/next")
    public void countryNext() {
        ContestService.nextCountry();
        sendUpdates();
    }

    @PutMapping("/countries/current/previous")
    public void countryPrevious() {
        ContestService.previousCountry();
        sendUpdates();
    }

    @GetMapping("/players/")
    public Collection<String> players() {
        return Arrays.stream(Player.values()).map(Enum::toString).toList();
    }

    @PutMapping("/players/{playerName}/afk")
    public void playerAfk(@PathVariable String playerName, @RequestBody boolean afk) {
        ContestService.setAfk(Player.valueOf(playerName), afk);
        sendUpdates();
    }

    @PutMapping("/votes/vote")
    public void addVote(@RequestBody VoteDto vote) {
        log.info("{} voted {} for {}", vote.getPlayer(), vote.getVote(), vote.getCountry());
        ContestService.addVote(Player.valueOf(vote.getPlayer()), vote.getCountry(), vote.getVote());
        sendUpdates();
    }

    @PutMapping("/votes/showCurrent")
    public void showCurrentVotes(@RequestBody String playerName) {
        log.info("{} called showCurrentVotes()", playerName);
        ContestService.showCurrentVotes();
        sendUpdates();
    }

    @GetMapping("/reset")
    public void reset() {
        log.info("RESET CALLED!");
        ContestState.reset();
        sendUpdates();
    }

    /**
     * Sends the current state, e.g. for initialization. For updates, use the SSE connection {@link #sseEventListener}.
     */
    @GetMapping(path = "/contest", produces = MediaType.APPLICATION_JSON_VALUE)
    public ContestUpdateDto contest() {
        return new ContestUpdateDto();
    }

    @GetMapping(path = "/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter sseEventListener() {
        log.info("New SSE connection");
        SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);
        emitters.add(emitter);

        emitter.onCompletion(() -> {
            log.info("SSE connection closed");
            emitters.remove(emitter);
        });
        emitter.onTimeout(() -> {
            log.info("SSE connection timeout");
            emitters.remove(emitter);
        });
        emitter.onError((e) -> {
            log.info("SSE connection error: {}", e.getMessage());
            emitters.remove(emitter);
        });

        return emitter;
    }

    private void sendUpdates() {
        emitters.forEach((emitter) -> {
            try {
                emitter.send(SseEmitter.event().name("message").data(new ContestUpdateDto()));
            } catch (IOException e) {
                emitter.completeWithError(e);
            }
        });
    }
}
