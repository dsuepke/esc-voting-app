/*
 * Copyright (c) 2023 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */
package net.suepke.eurovision;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EurovisionApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurovisionApplication.class, args);
    }
}
