package net.suepke.eurovision;

import net.suepke.eurovision.config.Country;
import net.suepke.eurovision.config.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * Holds a global (static) state of the current contest. This class contains all mutable state, while the Enums
 * {@link Country} and {@link Player} contain the immutable state that only changes with a new contest.
 */
public final class ContestState {

    /**
     * The votes by each player for each country. The extra/last two columns are providing the sum and average.
     */
    public static int[][] votes;
    /**
     * When a player adds a vote, it is being added to the current country.
     */
    public static Country currentCountry;
    /**
     * If true, the GUI should display the votes to everyone, even if not everyone has voted yet.
     */
    public static boolean showCurrentVotes;
    /**
     * Players can mark themselves as afk and are then not counted when checking whether everyone has voted.
     */
    public static Set<Player> afkPlayers;

    public static void reset() {
        votes = new int[Player.values().length + 2][Country.values().length];
        currentCountry = Country.values()[0];
        showCurrentVotes = false;
        afkPlayers = new HashSet<>();
    }

    static {
        reset();
    }
}
