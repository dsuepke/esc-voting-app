/*
 * Copyright (c) 2023 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision;

import lombok.Getter;
import net.suepke.eurovision.config.Country;
import net.suepke.eurovision.config.Player;

@Getter
public class ContestService {

    public static void nextCountry() {
        if (ContestState.currentCountry.ordinal() < Country.values().length - 1) {
            ContestState.showCurrentVotes = false;
            ContestState.currentCountry = Country.values()[ContestState.currentCountry.ordinal() + 1];
        }
    }

    public static void previousCountry() {
        if (ContestState.currentCountry.ordinal() > 0) {
            ContestState.showCurrentVotes = false;
            ContestState.currentCountry = Country.values()[ContestState.currentCountry.ordinal() - 1];
        }
    }

    public static void addVote(Player player, Country country, int vote) {
        ContestState.votes[player.ordinal()][country.ordinal()] = vote;

        int voted = 0;
        int sum = 0;

        for (int i = 0; i < Player.values().length; i++) {
            int currentVote = ContestState.votes[i][country.ordinal()];
            sum += currentVote;

            if (currentVote > 0) {
                voted++;
            }
        }

        ContestState.votes[Player.values().length][country.ordinal()] = sum;
        ContestState.votes[Player.values().length + 1][country.ordinal()] = sum / voted;
    }

    public static void showCurrentVotes() {
        ContestState.showCurrentVotes = true;
    }

    public static void setAfk(Player player, boolean afk) {
        if (afk) {
            ContestState.afkPlayers.add(player);
        } else {
            ContestState.afkPlayers.remove(player);
        }
    }
}
