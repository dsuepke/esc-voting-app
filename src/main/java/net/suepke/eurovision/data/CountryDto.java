/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.data;

import lombok.Getter;
import net.suepke.eurovision.config.Country;

@Getter
public class CountryDto {

	private final String name;
	private final String flag;
	private final int ordinal;

	public CountryDto(Country country) {
		name = country.toString();
		flag = country.getFlag();
		ordinal = country.ordinal();
	}
}
