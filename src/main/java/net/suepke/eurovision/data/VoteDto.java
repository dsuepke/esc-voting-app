/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.suepke.eurovision.config.Country;

@Getter
@AllArgsConstructor
public class VoteDto {

    private String player;
    private Country country;
    private int vote;

}

