package net.suepke.eurovision.data;

import lombok.Getter;
import net.suepke.eurovision.ContestState;
import net.suepke.eurovision.config.Player;

import java.util.HashSet;
import java.util.Set;

@Getter
public final class ContestUpdateDto {

    private final int[][] votes;
    private final CountryDto currentCountry;
    private final Set<String> currentPlayersVoted;
    private final boolean showCurrentVotes;
    private final Set<Player> afkPlayers;

    public ContestUpdateDto() {
        votes = ContestState.votes;
        currentCountry = new CountryDto(ContestState.currentCountry);
        currentPlayersVoted = playersVotedForCurrentCountry();
        showCurrentVotes = ContestState.showCurrentVotes;
        afkPlayers = ContestState.afkPlayers;
    }

    private static Set<String> playersVotedForCurrentCountry() {
        Set<String> playersVoted = new HashSet<>();
        for (Player player : Player.values()) {
            if (ContestState.votes[player.ordinal()][ContestState.currentCountry.ordinal()] != 0) {
                playersVoted.add(player.name());
            }
        }
        return playersVoted;
    }
}
