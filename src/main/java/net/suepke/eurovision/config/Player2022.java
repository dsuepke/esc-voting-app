/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.config;

@SuppressWarnings("unused")
enum Player2022 {

    DANIEL,
    MARKUS,
    CONNIE,
    NINA,
    JANIK,
    RALF,
    HIEMKE,
    ARP
}
