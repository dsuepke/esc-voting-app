/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.config;

public enum Player {

    DANIEL,
    ALEX,
    NINA,
    JANIK,
    RALF,
    HIEMKE,
    ARP,
    PADDI

}
