/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.config;

@SuppressWarnings("unused")
enum Player2023 {

    DANIEL,
    ALEX,
    NINA,
    JANIK,
    RALF,
    HIEMKE,
    ARP,
    SIRI,
    PADDI
}
