/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Contains all Country.values() of the current ESC final in order.
 */
@Getter
@SuppressWarnings("unused")
@AllArgsConstructor
enum Country2023 {

    AUSTRIA("🇦🇹"),
    PORTUGAL("🇵🇹"),
    SWITZERLAND("🇨🇭"),
    POLAND("🇵🇱"),
    SERBIA("🇷🇸"),
    FRANCE("🇫🇷"),
    CYPRUS("🇨🇾"),
    SPAIN("🇪🇸"),
    SWEDEN("🇸🇪"),
    ALBANIA("🇦🇱"),
    ITALY("🇮🇹"),
    ESTONIA("🇪🇪"),
    FINLAND("🇫🇮"),
    CZECH_REPUBLIC("🇨🇿"),
    AUSTRALIA("🇦🇺"),
    BELGIUM("🇧🇪"),
    ARMENIA("🇦🇲"),
    MOLDOVA("🇲🇩"),
    UKRAINE("🇺🇦"),
    NORWAY("🇳🇴"),
    GERMANY("🇩🇪"),
    LITHUANIA("🇱🇹"),
    ISRAEL("🇮🇱"),
    SLOVENIA("🇸🇮"),
    CROATIA("🇭🇷"),
    UNITED_KINGDOM("🇬🇧");

    /**
     * Country flag as unicode string.
     */
    final String flag;
}
