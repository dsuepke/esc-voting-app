/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Contains all Country.values() of the current ESC final in order.
 */
@Getter
@SuppressWarnings("unused")
@AllArgsConstructor
enum Country2022 {

    CZECH_REPUBLIC("🇨🇿"),
    ROMANIA("🇷🇴"),
    PORTUGAL("🇵🇹"),
    FINLAND("🇫🇮"),
    SWITZERLAND("🇨🇭"),
    FRANCE("🇫🇷"),
    NORWAY("🇳🇴"),
    ARMENIA("🇦🇲"),
    ITALY("🇮🇹"),
    SPAIN("🇪🇸"),
    NETHERLANDS("🇳🇱"),
    UKRAINE("🇺🇦"),
    GERMANY("🇩🇪"),
    LITHUANIA("🇱🇹"),
    AZERBAIJAN("🇦🇿"),
    BELGIUM("🇧🇪"),
    GREECE("🇬🇷"),
    ICELAND("🇮🇸"),
    MOLDOVA("🇲🇩"),
    SWEDEN("🇸🇪"),
    AUSTRALIA("🇦🇺"),
    UK("🇬🇧"),
    POLAND("🇵🇱"),
    SERBIA("🇷🇸"),
    ESTONIA("🇪🇪");

    /**
     * Country flag as unicode string.
     */
    final String flag;
}
