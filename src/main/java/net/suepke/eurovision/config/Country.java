/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

package net.suepke.eurovision.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Contains all Country.values() of the current ESC final in order.
 */
@Getter
@AllArgsConstructor
public enum Country {

    SWEDEN("🇸🇪"),
    UKRAINE("🇺🇦"),
    GERMANY("🇩🇪"),
    LUXEMBOURG("🇱🇺"),
    ISRAEL("🇮🇱"),
    LITHUANIA("🇱🇹"),
    SPAIN("🇪🇸"),
    ESTONIA("🇪🇪"),
    IRELAND("🇮🇪"),
    LATVIA("🇱🇻"),
    GREECE("🇬🇷"),
    UNITED_KINGDOM("🇬🇧"),
    NORWAY("🇳🇴"),
    ITALY("🇮🇹"),
    SERBIA("🇷🇸"),
    FINLAND("🇫🇮"),
    PORTUGAL("🇵🇹"),
    ARMENIA("🇦🇲"),
    CYPRUS("🇨🇾"),
    SWITZERLAND("🇨🇭"),
    SLOVENIA("🇸🇮"),
    CROATIA("🇭🇷"),
    GEORGIA("🇬🇪"),
    FRANCE("🇫🇷"),
    AUSTRIA("🇦🇹");

    /**
     * Country flag as unicode string.
     */
    final String flag;

}
