/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

//
// rest.js
//
// Provides mirror methods for the API provided by the server.
//

async function vote(rating) {
    ELEM_VOTING.style.opacity = '0.3';

    await serverPUT('votes/vote', {
        player: player,
        country: currentCountry.name,
        vote: rating
    });
    await updateUI();

    ELEM_VOTING.style.opacity = '1';
}

function countryNext() {
    serverPUT('countries/current/next')
    window.scrollTo(0, 0);
}

function countryPrevious() {
    serverPUT('countries/current/previous')
    window.scrollTo(0, 0);
}

async function setShowCurrentVotes() {
    serverPUT('votes/showCurrent', player);
    window.scrollTo(0, 0);
}

async function goAfk() {
    serverPUT("players/" + player + "/afk", "true");
}

async function stopAfk() {
    serverPUT("players/" + player + "/afk", "false");
}