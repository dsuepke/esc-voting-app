/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

//
// ui.js
//
// Provides methods for updating the ui based on current state/data in main.js
//


// TODO: Use css classes
function backgroundColorForVote(vote) {
    if (vote <= 4) {
        return "background-color:#fcc";
    }
    if (vote >= 5 && vote <= 8) {
        return "background-color:#ffc";
    }
    return "background-color:#cfc";
}


/**
 * Updates the UI based on the local data (should be synced with server prior).
 */
function updateUI() {
    ELEM_COUNTRYFLAG.textContent = currentCountry.flag;
    ELEM_COUNTRYNAME.textContent = toTitleCase(currentCountry.name);

    uiUpdateSelectedTab();
    uiUpdatePlayerSelected();
    uiUpdatePlayerDropdown();
    uiUpdatecurrentPlayersVoted();
    uiUpdateVotes();
}

function uiUpdateSelectedTab() {
    ELEM_TAB_VOTING.style.display = "none";
    ELEM_TAB_RESULTS.style.display = "none";
    ELEM_TAB_SETTINGS.style.display = "none"
    ELEM_TAB_AFK.style.display = "none"
    ELEM_TABSELECTOR_VOTING.className = "";
    ELEM_TABSELECTOR_RESULTS.className = "";
    ELEM_TABSELECTOR_SETTINGS.className = "";
    if (!player) {
        return;
    }
    switch (selectedTab) {
        case 0:
            ELEM_TAB_VOTING.style.display = "";
            ELEM_TABSELECTOR_VOTING.className = "active";
            return;
        case 1:
            ELEM_TAB_RESULTS.style.display = "";
            ELEM_TABSELECTOR_RESULTS.className = "active";
            return;
        case 2:
            ELEM_TAB_SETTINGS.style.display = "";
            ELEM_TABSELECTOR_SETTINGS.className = "active";
            return;
        case 3:
            ELEM_TAB_AFK.style.display = "";
            return;
    }
}

function uiUpdatePlayerSelected() {
    if (player && !afkPlayers.includes(player)) {
        ELEM_TABS.style.display = "";
    } else {
        ELEM_TABS.style.display = "none";
    }
}

function uiUpdatePlayerDropdown() {
    if (player) {
        ELEM_PLAYERDROPDOWN_CONTAINER.style.display = "none";
        return;
    }

    playerDropDownHTML = '<option disabled="" selected="" value=""> -- Auswählen --</option>';

    for (let i = 0; i < players.length; i++) {
        playerDropDownHTML += '<option>' + toTitleCase(players[i]) + '</option>';
    }

    if (ELEM_PLAYERDROPDOWN.innerHTML !== playerDropDownHTML) {
        ELEM_PLAYERDROPDOWN.innerHTML = playerDropDownHTML;
    }
}

function selectTab() {

}


function uiUpdatecurrentPlayersVoted() {
    let currentPlayersVotedText = '';
    currentPlayersVotedText += '<table style="margin:0; padding: 0;"><tbody>'

    for (let i = 0; i < players.length; i++) {
        let playerVoted = false;
        for (let j = 0; j < currentPlayersVoted.length && !playerVoted; j++) {
            if (players[i] === currentPlayersVoted[j]) {
                playerVoted = true;
            }
        }

        let playerAfk = false;
        for (let j = 0; j < afkPlayers.length && !playerAfk; j++) {
            if (players[i] === afkPlayers[j]) {
                playerAfk = true;
            }
        }

        if (everyoneVoted() || showCurrentVotes || (players[i] === player && votes[i][currentCountry.ordinal] !== 0)) {
            if (votes[i][currentCountry.ordinal] === 0) {
                currentPlayersVotedText += '<tr><td>' + toTitleCase(players[i]) + ':</td><td style="text-align: right;">-</td></tr>';
            } else {
                currentPlayersVotedText += '<tr><td>' + toTitleCase(players[i]) + ':</td><td style="text-align: right;' + backgroundColorForVote(votes[i][currentCountry.ordinal]) + '">' + votes[i][currentCountry.ordinal] + '</td></tr>';
            }
        } else if (playerVoted) {
            currentPlayersVotedText += '<tr><td colspan="2" style="color:green;">' + toTitleCase(players[i]) + ' has voted</td></tr>';
        } else if (playerAfk) {
            currentPlayersVotedText += '<tr><td colspan="2" style="color:grey;">' + toTitleCase(players[i].toLowerCase()) + ' is afk</td></tr>';
        } else {
            currentPlayersVotedText += '<tr><td colspan="2" style="color:red;">' + toTitleCase(players[i].toLowerCase()) + ' is voting...</td></tr>';
        }
    }

    currentPlayersVotedText += '</tbody></table>';

    if (ELEM_currentPlayersVoted.innerHTML !== currentPlayersVotedText) {
        ELEM_currentPlayersVoted.innerHTML = currentPlayersVotedText;
    }
}

function uiUpdateVotes() {
    let votesString = '<table class="overviewTable">';
    votesString += "<thead><tr><th></th>";

    for (let i = 0; i < players.length + 2; i++) {
        if (i < players.length) {
            votesString += "<th>" + toTitleCase(players[i]) + "</th>";
        } else if (i < players.length + 1) {
            votesString += "<th>∑</th>";
        } else {
            votesString += "<th>x̅</th>";
        }
    }
    votesString += "</tr></thead>";
    votesString += "<tbody>";

    for (let i = 0; i < countries.length; i++) {
        if (currentCountry.ordinal === i && !everyoneVoted()) {
            votesString += '<tr style="background-color: lightgoldenrodyellow">';
        } else {
            votesString += '<tr>';
        }
        votesString += '<th style="text-align: left;">' + toTitleCase(countries[i]) + "</th>";

        for (let j = 0; j < players.length + 2; j++) {
            if (currentCountry.ordinal === i && !everyoneVoted() && !showCurrentVotes) {
                votesString += "<td>???</td>";
            } else if (votes[j][i] === undefined || votes[j][i] === 0) {
                votesString += "<td></td>";
            } else {
                votesString += '<td style="' + backgroundColorForVote(votes[j][i]) + '">' + votes[j][i] + '</td>';
            }
        }
        votesString += "</tr>"
    }
    votesString += "</tbody></table>"

    if (ELEM_VOTES.innerHTML !== votesString) {
        ELEM_VOTES.innerHTML = votesString;
    }
}
