/*
 * Copyright (c) 2022 by Daniel Süpke.
 *
 * Licensed under GPLv3, see LICENSE.md or https://www.gnu.org/licenses/.
 */

//
// main.js
//
// Provides the main control methods and state as well as helper methods.
//


const BASE_URL = "https://esc.suepke.net/"
//const BASE_URL = "http://localhost:8081/"

const UPDATE_INTERVAL = 300; // Update from server every n milliseconds

// Initialized once (from server)
let players = [];
let countries = [];

// Continuously updated from server
let votes;
let currentCountry;
let showCurrentVotes = false;
let currentPlayersVoted = [];
let afkPlayers = [];
let player;
let selectedTab = 0;

// HTML elements
const ELEM_COUNTRYFLAG = document.getElementById("currentCountry.flag");
const ELEM_COUNTRYNAME = document.getElementById("currentCountry.name");
const ELEM_TAB_VOTING = document.getElementById('tabVoting');
const ELEM_TAB_RESULTS = document.getElementById('tabResults');
const ELEM_TAB_SETTINGS = document.getElementById('tabSettings');
const ELEM_TAB_AFK = document.getElementById('tabAfk');
const ELEM_TABSELECTOR_VOTING = document.getElementById('tabSelect0')
const ELEM_TABSELECTOR_RESULTS = document.getElementById('tabSelect1')
const ELEM_TABSELECTOR_SETTINGS = document.getElementById('tabSelect2')
const ELEM_PLAYERDROPDOWN = document.getElementById('player');
const ELEM_PLAYERDROPDOWN_CONTAINER = document.getElementById('playerSelectContainer');
const ELEM_currentPlayersVoted = document.getElementById("currentPlayersVoted");
const ELEM_VOTES = document.getElementById("votes");
const ELEM_VOTING = document.getElementById('voting');
const ELEM_TABS = document.getElementById("tabs");


/**
 * Sends a PUT request to the server with the relative path. Helper function.
 * @param path Relative path to be used with BASE_URL
 * @param requestBody An object containing the call parameters
 */
function serverPUT(path, requestBody) {
    if (requestBody === undefined) {
        requestBody = {};
    }
    console.info(player + ' calling ' + path + ' with requestBody: ', JSON.stringify(requestBody));

    fetch(BASE_URL + path, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(requestBody)
    });
}

function updateData(data) {
    votes = data.votes;
    showCurrentVotes = data.showCurrentVotes;
    currentCountry = data.currentCountry;
    currentPlayersVoted = data.currentPlayersVoted;
    afkPlayers = data.afkPlayers;
}

/**
 * Sets the current player based on the dropdown's selection.
 */
function selectPlayer() {
    console.debug('Selected player ' + ELEM_PLAYERDROPDOWN.value.toUpperCase());
    player = ELEM_PLAYERDROPDOWN.value.toUpperCase();
    if (afkPlayers.includes(player)) {
        selectedTab = 3;
    }
    updateUI();
}

/**
 * Checks whether everyone has finished voting for the current country.
 */
function everyoneVoted() {
    const combinedArray = [...new Set([...currentPlayersVoted, ...afkPlayers])];
    return combinedArray.length === players.length;
}

/**
 * Makes first letter of a given string uppercase, the rest lowercase.
 * E.g. HELLO -> Hello
 */
function toTitleCase(string) {
    // Replace all underscores with spaces
    let cleanString = string.replace(/_/g, ' ');
    // Capitalize the first letter of each word and make all other letters lowercase
    return cleanString.replace(/\w\S*/g, function (text) {
        return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
    });
}


function fetchInitialData() {
    countriesPromise = fetch(BASE_URL + 'countries/').then(response => response.json());
    playersPromise = fetch(BASE_URL + 'players/').then(response => response.json());
    contestPromise = fetch(BASE_URL + 'contest').then(response => response.json());
    return Promise.all([countriesPromise, playersPromise, contestPromise]);
}

fetchInitialData().then(data => {
    countries = data[0];
    players = data[1];
    updateData(data[2]);
    updateUI();

    const eventSource = new EventSource(BASE_URL + "events");
    eventSource.onmessage = (event) => {
        console.debug("Received new state data");
        updateData(JSON.parse(event.data));
        updateUI();
    };
    eventSource.onerror = function (event) {
        console.error("EventSource failed:", event);
    };
}).catch(error => {
    console.error('Failed to fetch initial data:', error);
});
