# ESC Voting app

A small project to vote with a couple of friends on the ESC performers. Votes will be displayed once everonye voted.

Uses Spring Boot with a central REST service and a static html site for easy deployment.

## Config

* Server URL is hardcoded in `main.js`
* Players and Country.values() are hardcoded in their respective Enums